CREATE TYPE TEXT AS character(1000000);

------------------ Création de la table dossier_partenaire ------------------
CREATE TABLE IF NOT EXISTS dossier_partenaire
(
  uuid character(36) NOT NULL,
  numerodossier character(20) NOT NULL,
  codeedi character varying(30) NOT NULL,
  etat character varying(20) NOT NULL,
  contenu_xml text NOT NULL,
  role character varying(7) DEFAULT 'NR' NOT NULL,
  indicesousdossier character varying(5),
  nom_fichier_depose character varying(255),
  chemin_fichier_depose character varying(255),
  archiver boolean NOT NULL,
  numero_liasse character varying(255) DEFAULT 'NA' NOT NULL,
  type_flux character(1) DEFAULT '-' NOT NULL,
  ch_type character varying(10),
  CONSTRAINT dossier_partenaire_pkey PRIMARY KEY (uuid)
);

