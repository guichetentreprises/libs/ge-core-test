----CREATE TYPE TEXT AS character(1000000);

------------------ Création de la table Etat ------------------
CREATE TABLE IF NOT EXISTS etat
(
  id character varying(10) NOT NULL,
  libelle character varying(200),
  CONSTRAINT etat_pkey PRIMARY KEY (id)
);

------------------ Création de la table FORMALITE_XML ------------------
CREATE TABLE IF NOT EXISTS dossier
(
  id bigint NOT NULL,
  etatid character varying(10),
  utilisateurid character varying(18),
  datecreation timestamp(6) without time zone,
  datemodification timestamp(6) without time zone,
  numerodossier character varying(255),
  nom character varying(255),
  dernier_fichier_ses character varying(255),
  profilactivite character varying(420),
  datedelete date,
  etat_signe integer DEFAULT 0 NOT NULL ,
  numeroliasse character varying(255) DEFAULT 'N/A' NOT NULL ,
  CONSTRAINT dossier_pkey PRIMARY KEY (id),
  CONSTRAINT fkdossier28858 FOREIGN KEY (etatid)
      REFERENCES etat (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);

------------------ Création de la table FORMALITE_XML ------------------

CREATE TABLE IF NOT EXISTS formalite_xml
(
  id character varying(36) NOT NULL,
  identifiant_formalite character varying(50) NOT NULL,
  contenu_xml character varying(100000) NOT NULL,
  numero_dossier character(20),
  date_creation date NOT NULL,
  date_mise_a_jour date,
  user_id character varying(30) NOT NULL,
  version character varying(13) NOT NULL,
  model_version character varying(13),
  CONSTRAINT formalite_xml_pkey PRIMARY KEY (id)
);

------------------ Création de la table DOCUMENT_CERFA ------------------
CREATE TABLE IF NOT EXISTS document_cerfa
(
  numero_dossier character varying(100) NOT NULL, -- Numero de dossier du document signe
  chemin_fichier character varying(130) NOT NULL, -- Chemin de stockage du fichier sur le serveur
  avec_signature boolean NOT NULL, -- Le document est il signé ou non
  date_signature date, -- Date de signature du fichier par DTP
  id bigint NOT NULL, -- Identifiant technique
  CONSTRAINT document_cerfa_numero_dossier UNIQUE (numero_dossier)
);

------------------ Création de la table piece_justificative_xml ------------------
CREATE TABLE IF NOT EXISTS piece_justificative_xml
(
  id character varying(50) NOT NULL,
  id_pj character varying(100) NOT NULL,
  identifiant_formalite_liee character varying(100) NOT NULL,
  numero_dossier character varying(255) NOT NULL,
  contenu_xml character varying(1048576) NOT NULL,
  date_creation date,
  date_mise_a_jour date,
  user_id character varying(50) NOT NULL,
  version character varying(50),
  model_version character varying(50) NOT NULL,
  CONSTRAINT piece_justificative_xml_pkey PRIMARY KEY (id)
);

------------------ Création de la table paiement_formalite ------------------
CREATE TABLE IF NOT EXISTS paiement_formalite
(
  idformalite character varying(13) NOT NULL,
  etat_id character varying(13) NOT NULL,
  contenu character varying(100000) NOT NULL,
  idtechnique bigint NOT NULL,
  historise boolean DEFAULT false NOT NULL ,
  CONSTRAINT pk_paiementformalite PRIMARY KEY (idtechnique)
);

------------------ Création de la table paiementencours ------------------
CREATE TABLE IF NOT EXISTS paiementencours
(
  id bigint NOT NULL ,
  dossierid bigint,
  numformalite character varying(13),
  reference character varying(15),
  dateenvoi timestamp(6) without time zone,
  dateretour timestamp(6) without time zone,
  etat_id character varying(2),
  montant bigint,
  destinataire character varying(10),
  CONSTRAINT paiementencours_pkey PRIMARY KEY (id)
);

------------------ Création de la table paiement_effectue ------------------
CREATE TABLE IF NOT EXISTS paiement_effectue
(
  id bigint NOT NULL,
  numero_formalite character varying(13),
  reference character varying(15),
  code_destinataire character varying(30),
  montant bigint,
  date_retour timestamp(6) without time zone,
  CONSTRAINT paiement_effectue_pkey PRIMARY KEY (id)
);

------------------ Création de la table documentssignes ------------------
CREATE TABLE IF NOT EXISTS documentssignes
(
  id integer NOT NULL, -- identifiant autoincremental
  numero_dossier character varying(100), -- numero de dossier du document signe
  source character varying(100), -- Chemin de stockage du fichier signe par DTP
  date_signature date, -- Date de signature du fichier par DTP
  CONSTRAINT id PRIMARY KEY (id),
  CONSTRAINT signature_numero_formalite_key UNIQUE (numero_dossier)
);

------------------ Création de la table destinataire_dossier ------------------
CREATE TABLE IF NOT EXISTS destinataire_dossier
(
  id bigint NOT NULL,
  code_edi character varying(5) NOT NULL,
  id_dossier bigint NOT NULL,
  role character varying(10) NOT NULL,
  CONSTRAINT destinataire_dossier_pkey PRIMARY KEY (id)
);

------------------ Création de la table rapport_exec ------------------
CREATE TABLE IF NOT EXISTS rapport_exec
(
  id character varying(20) NOT NULL,
  numero_dossier character varying(255) NOT NULL,
  cle_execution character varying(255) NOT NULL,
  id_execute character varying(255) NOT NULL,
  eval character varying(30),
  occurence character varying(30),
  libelle character varying(30),
  status character varying(30) NOT NULL,
  CONSTRAINT rapport_exec_pkey PRIMARY KEY (id)
);

------------------ Création de la table trace_operation ------------------
CREATE TABLE IF NOT EXISTS trace_operation
(
  id bigint NOT NULL, -- Identifiant technique
  date_action date NOT NULL, -- Date et heure de l'action
  ref_action character varying(64) NOT NULL, -- Référence de l'action ...
  numero_dossier character varying(255) NOT NULL, -- Numéro du dossier
  nom character varying(120) NOT NULL, -- Nom de la personne ayant effectué l'action
  prenom character varying(120) NOT NULL, -- Prénom de la personne ayant effectué l'action
  date_naissance character varying(10), -- Date de naissance de la personne ayant effectué l'action au format YYYY/MM/DD
  ip_source character varying(32), -- Adresse IP source de l'utilisateur issue des entetes HTTP fournies par le CIRTIL
  suppression boolean DEFAULT false NOT NULL , -- Booléen de suppression pour la purge
  date_suppression date -- Date de suppression
);

------------------ Création de la table traitementenattente ------------------
CREATE TABLE IF NOT EXISTS traitementenattente
(
  id bigint NOT NULL,
  dossier bigint,
  dateenregistrement timestamp without time zone,
  datedebuttraitement timestamp without time zone,
  datefintraitement timestamp without time zone,
  etat integer,
  CONSTRAINT traitementenattente_pkey PRIMARY KEY (id)
);

------------------ Création de la table historiquedossier ------------------
CREATE TABLE IF NOT EXISTS historiquedossier
(
  id bigint NOT NULL,
  dossierid bigint,
  libelle character varying(255),
  "Date" date,
  dossierindex integer,
  CONSTRAINT historiquedossier_pkey PRIMARY KEY (id),
  CONSTRAINT fkhistorique875078 FOREIGN KEY (dossierid)
      REFERENCES dossier (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);

------------------ Création de la table historiquetechniquedossier ------------------
CREATE TABLE IF NOT EXISTS historiquetechniquedossier
(
  id bigint NOT NULL,
  numerodossier character varying(255) NOT NULL,
  etatid character varying(10) NOT NULL,
  date timestamp without time zone NOT NULL,
  CONSTRAINT historiquetechniquedossier_pkey PRIMARY KEY (id),
  CONSTRAINT etatid_fkey FOREIGN KEY (etatid)
      REFERENCES etat (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);



------------------ Création de la table regimefiscal ------------------
CREATE TABLE regimefiscal
(
  id bigint NOT NULL,
  regimeimpositionbenefices character varying(100),
  regimeimpositionbeneficesversementliberatoire boolean,
  regimeimpositionbeneficesoptionsparticulieres character varying(100),
  regimeimpositiontva character varying(100),
  regimeimpositiontvaoptionsparticulieres1 boolean,
  regimeimpositiontvaoptionsparticulieres2 boolean,
  regimeimpositiontvaoptionsparticulieres3 boolean,
  regimeimpositiontvaoptionsparticulieres4 boolean,
  regimeimpositiontvaconditionversement boolean,
  dateclotureexercicecomptable date,
  enregistrementprealablestatuts boolean,
  enregistrementprealablestatutslieu character varying(32),
  enregistrementprealablestatutsdate date,
  CONSTRAINT regimefiscal_pkey PRIMARY KEY (id)
);

------------------ Création de la table eirl ------------------
CREATE TABLE eirl
(
  id bigint NOT NULL,
  regimefiscalid2 bigint,
  regimefiscalid bigint,
  statuteirl character varying(100),
  denomination character varying(120),
  dateclotureexercicecomptableeirl date,
  objet character varying(420),
  objetportantsurensembleactivites boolean,
  activitesaccessoiresbicbnc character varying(100),
  registredepot integer,
  precedenteirldenomination character varying(120),
  precedenteirlregistre character varying(50),
  precedenteirllieuimmatriculation character varying(35),
  precedenteirlsiren character varying(9),
  CONSTRAINT eirl_pkey PRIMARY KEY (id),
  CONSTRAINT fkeirl992706 FOREIGN KEY (regimefiscalid2)
      REFERENCES regimefiscal (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT regimefiscal FOREIGN KEY (regimefiscalid)
      REFERENCES regimefiscal (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);

------------------ Création de la table declarationarchitecte ------------------
CREATE TABLE declarationarchitecte
(
  id bigint NOT NULL,
  bieninteretpersonnel boolean,
  bieninteretpersonnel2 boolean,
  bieninteretprofessionnel boolean,
  bieninteretprofessionnel2 boolean,
  CONSTRAINT declarationarchitecte_pkey PRIMARY KEY (id)
);

------------------ Création de la table pays ------------------
CREATE TABLE pays
(
  id bigint NOT NULL,
  libelle character varying(38),
  nationalite character varying(100),
  codepays character varying(3),
  CONSTRAINT pays_pkey PRIMARY KEY (id)
);

------------------ Création de la table commune ------------------
CREATE TABLE commune
(
  id bigint NOT NULL,
  libelle character varying(32),
  codecommune character varying(5),
  codepostal character varying(9),
  arrondissement integer,
  CONSTRAINT commune_pkey PRIMARY KEY (id)
);

------------------ Création de la table adresse ------------------
CREATE TABLE adresse
(
  id bigint NOT NULL,
  communeid integer,
  paysid integer,
  numerovoie character varying(9),
  indicerepetition character varying(1),
  typevoie character varying(4),
  nomvoie character varying(32),
  complementadresse character varying(38),
  distributionspeciale character varying(38),
  libellelocalite character varying(32),
  codedepartement character varying(3),
  libelledepartement character varying(100),
  telephone1 character varying(14),
  telephone2 character varying(14),
  fax character varying(14),
  courriel character varying(80),
  siteweb character varying(100),
  formationid bigint,
  CONSTRAINT adresse_pkey PRIMARY KEY (id),
  CONSTRAINT fkadresse338120 FOREIGN KEY (paysid)
      REFERENCES pays (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fkadresse668333 FOREIGN KEY (communeid)
      REFERENCES commune (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);

------------------ Création de la table etablissement ------------------
CREATE TABLE etablissement
(
  id bigint NOT NULL,
  adresseid2 bigint,
  regimefiscalid2 bigint,
  regimefiscalid bigint,
  adresseid bigint,
  situation character varying(100),
  categorie character varying(100),
  domiciliation boolean,
  datedebutactivite date,
  activitepermanentesaisonniere character varying(1),
  periodeactivitesaisonniere1datedebut date,
  periodeactivitesaisonniere1datefin date,
  periodeactivitesaisonniere2datedebut date,
  periodeactivitesaisonniere2datefin date,
  nonsedentaritequalite character varying(15),
  estambulantue boolean,
  activites character varying(420),
  secteursactivites character varying(140),
  activitesexerceesagricole character varying(255),
  activiteplusimportante character varying(140),
  secteuractiviteplusimportant character varying(100),
  activiteplusimportanteagricole character varying(140),
  activiteplusimportanteagricoleautre character varying(255),
  estactiviteliberale boolean,
  activiteviticole boolean,
  activiteelevage boolean,
  activitesaccessoiresbicbnc character varying(20),
  aqpasituation character varying(1),
  aqpaintitulediplome character varying(120),
  activitenature character varying(2),
  activitenatureautre character varying(50),
  activitelieuexercice integer,
  activitelieuexerciceautre character varying(20),
  activitesurfacemagasin integer,
  nomcommercialprofessionnel character varying(100),
  enseigne character varying(50),
  originefonds character varying(100),
  precedentexploitantnombre integer,
  loueurmandantdufondsdebutcontrat date,
  loueurmandantdufondsdureecontratindeterminee boolean,
  loueurmandantdufondsfincontrat date,
  loueurmandantdufondsrenouvellementtacitereconduction boolean,
  plancession boolean,
  fondsuniquementartisanal boolean,
  journalannonceslegalesnom character varying(35),
  journalannonceslegalesdateparution date,
  effectifsalarieembauchepremiersalarie integer,
  effectifsalarienombre integer,
  effectifsalarieagricolenombre integer,
  effectifsalarieapprentis integer,
  effectifsalariepresence boolean,
  personnepouvoirpresence boolean,
  personnepouvoirnombre integer,
  activiteimmobiliere character varying(200),
  inscriptionrsac boolean,
  activitesexerceesagricole1 boolean,
  activitesexerceesagricole2 boolean,
  activitesexerceesagricole3 boolean,
  activitesexerceesagricole4 boolean,
  activiteestexerceadressesiege boolean,
  CONSTRAINT etablissement_pkey PRIMARY KEY (id),
  CONSTRAINT etablissementadresse FOREIGN KEY (adresseid2)
      REFERENCES adresse (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fketablissem202007 FOREIGN KEY (regimefiscalid)
      REFERENCES regimefiscal (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fketablissem838167 FOREIGN KEY (regimefiscalid2)
      REFERENCES regimefiscal (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);

------------------ Création de la table organedirection ------------------
CREATE TABLE organedirection
(
  id bigint NOT NULL,
  CONSTRAINT organedirection_pkey PRIMARY KEY (id)
);

------------------ Création de la table personnephysique ------------------
CREATE TABLE personnephysique
(
  id bigint NOT NULL,
  adresseid4 bigint,
  adresseid3 bigint,
  adresseid2 bigint,
  adresseid bigint,
  nomnaissance character varying(100),
  nomusage character varying(100),
  pseudonyme character varying(255),
  datenaissance date,
  civilite character varying(5),
  nationalite character varying(255),
  mineuremancipe boolean,
  prenom1 character varying(255),
  prenom2 character varying(255),
  prenom3 character varying(255),
  prenom4 character varying(255),
  lieunaissance character varying(255),
  profession character varying(255),
  organedirectionid bigint,
  organedirectionindex integer,
  CONSTRAINT personnephysique_pkey PRIMARY KEY (id),
  CONSTRAINT fk3a865bd67420f736 FOREIGN KEY (organedirectionid)
      REFERENCES organedirection (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fkpersonneph220329 FOREIGN KEY (adresseid3)
      REFERENCES adresse (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fkpersonneph220330 FOREIGN KEY (adresseid4)
      REFERENCES adresse (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);

------------------ Création de la table personnemorale ------------------
CREATE TABLE personnemorale
(
  id bigint NOT NULL,
  adresseid2 bigint,
  adresseid bigint,
  personnephysiqueid bigint,
  denomination character varying(120),
  sigle character varying(255),
  formejuridique character varying(255),
  numeroidentification character varying(255),
  lieuimmatriculation character varying(255),
  representantqualite character varying(50),
  CONSTRAINT personnemorale_pkey PRIMARY KEY (id),
  CONSTRAINT fkpersonnemo73401 FOREIGN KEY (adresseid2)
      REFERENCES adresse (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fkpersonnemo934419 FOREIGN KEY (personnephysiqueid)
      REFERENCES personnephysique (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);


------------------ Création de la table entrepriseliee ------------------
CREATE TABLE entrepriseliee
(
  id bigint NOT NULL,
  entrepriseid2 bigint,
  adresseid2 bigint,
  entrepriseid bigint,
  personnemoraleid bigint,
  personnephysiqueid bigint,
  adresseid bigint,
  etablissementid bigint,
  nature integer,
  nom character varying(120),
  siren character varying(9),
  numerodetenteur bigint,
  numeroexploitation bigint,
  typelien character varying(100),
  etablissementindex integer,
  entrepriseindex integer,
  "Column" integer,
  entrepriseid3 bigint,
  formejuridique character varying(50),
  greffeimmatriculation character varying(5),
  domiciliatairememegreffe boolean,
  CONSTRAINT entrepriseliee_pkey PRIMARY KEY (id),
  CONSTRAINT fkentreprise691488 FOREIGN KEY (adresseid2)
      REFERENCES adresse (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fkentreprise830469 FOREIGN KEY (personnephysiqueid)
      REFERENCES personnephysique (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fkentreprise978831 FOREIGN KEY (personnemoraleid)
      REFERENCES personnemorale (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);

------------------ Création de la table personne ------------------
CREATE TABLE personne
(
  id bigint NOT NULL,
  personnemoraleid bigint,
  personnephysiqueid bigint,
  CONSTRAINT personne_pkey PRIMARY KEY (id),
  CONSTRAINT fkpersonne283721 FOREIGN KEY (personnephysiqueid)
      REFERENCES personnephysique (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fkpersonne432083 FOREIGN KEY (personnemoraleid)
      REFERENCES personnemorale (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);

------------------ Création de la table conjoint ------------------
CREATE TABLE conjoint
(
  id bigint NOT NULL,
  personneid bigint,
  CONSTRAINT conjoint_pkey PRIMARY KEY (id),
  CONSTRAINT fkconjoint873123 FOREIGN KEY (personneid)
      REFERENCES personne (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);

------------------ Création de la table entreprise ------------------
CREATE TABLE entreprise
(
  id bigint NOT NULL,
  entrepriselieeid3 bigint,
  entrepriselieeid2 bigint,
  adresseid bigint,
  conjointid bigint,
  eirlid bigint,
  entrepriselieeid bigint,
  etablissementid bigint,
  formejuridique character varying(10),
  statutlegalparticulier character varying(255),
  denomination character varying(255),
  sigle character varying(255),
  activitesprincipales character varying(420),
  duree integer,
  capitalmontant bigint,
  capitalvariable boolean,
  capitalvariableminimum bigint,
  dateagrementgaec date,
  dateclotureexercicesocial date,
  datecloturepremierexercicesocial date,
  associeunique boolean,
  associeuniqueestpresident boolean,
  statutstypes boolean,
  fusionscission integer,
  fusionscissionnombre integer,
  nombredirigeants integer,
  personnepouvoirpresence boolean,
  personnepouvoirnombre integer,
  naturegerance integer,
  naturegerancesocieteassociee boolean,
  effectifsalarienombre integer,
  estactiviteartisanaleprincipale boolean,
  activiteartisanaleoption integer,
  estinscritrm boolean,
  insaisissabilitedeclaration boolean,
  insaisissabilitepublication character varying(35),
  contratappuipresence boolean,
  contratappuidatefin date,
  autreetablissementuepresence boolean,
  autreetablissementuenombre integer,
  demandeaccre boolean,
  adresseentrepriseppsituation integer,
  adresseentreprisepmsituation integer,
  capitaldevise character varying(5),
  capitalapportnature character varying(25),
  capitalapportsnaturesuperieur30000 boolean,
  capitalapportsnaturesuperieurmoitiecapital boolean,
  proprietaireterreexploite boolean,
  naturegroupementpastoral character varying(255),
  estinscritrcs boolean,
  exercicedoubleactivite boolean,
  informationdepotfonds boolean,
  CONSTRAINT entreprise_pkey PRIMARY KEY (id),
  CONSTRAINT eirl FOREIGN KEY (eirlid)
      REFERENCES eirl (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT entreprisesliees FOREIGN KEY (entrepriselieeid)
      REFERENCES entrepriseliee (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT etablissements FOREIGN KEY (etablissementid)
      REFERENCES etablissement (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fkentreprise113243 FOREIGN KEY (adresseid)
      REFERENCES adresse (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fkentreprise405507 FOREIGN KEY (entrepriselieeid3)
      REFERENCES entrepriseliee (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fkentreprise405508 FOREIGN KEY (entrepriselieeid2)
      REFERENCES entrepriseliee (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fkentreprise7167 FOREIGN KEY (conjointid)
      REFERENCES conjoint (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);

------------------ Création de la table observation ------------------
CREATE TABLE observation
(
  id bigint NOT NULL,
  commentaire character varying(300),
  CONSTRAINT observation_pkey PRIMARY KEY (id)
);

------------------ Création de la table evenement ------------------
CREATE TABLE evenement
(
  id character varying(10) NOT NULL,
  libelle character varying(200),
  typeevenement character varying(9) DEFAULT 'formalite' NULL,
  optionaffichage  character varying(3) DEFAULT 111 NOT NULL ,
  CONSTRAINT evenement_pkey PRIMARY KEY (id)
);



------------------ Création de la table Formalité ------------------
CREATE TABLE formalite
(
  adresseid6 bigint,
  adresseid5 bigint,
  id character varying(13) NOT NULL,
  adresseid4 bigint,
  adresseid3 bigint,
  etatid character varying(10),
  dossierid bigint,
  observationid bigint,
  adresseid2 bigint,
  evenementid character varying(10) NOT NULL,
  adresseid bigint,
  entrepriseid bigint NOT NULL,
  reseaucfe character varying(10),
  estautoentrepreneur boolean,
  optioneirl boolean,
  telephone1 character varying(14),
  telephone2 character varying(14),
  fax character varying(14),
  courriel character varying(80),
  signatairenom character varying(100),
  signatairequalite character varying(20),
  signaturedate date,
  signaturelieu character varying(32),
  correspondancedestinataire character varying(255),
  dateenvoi timestamp(6) without time zone,
  dossierindex integer,
  cfecompetent character varying(10),
  codecommuneactivite character varying(5),
  agentcommercial boolean,
  ambulant boolean,
  termine boolean,
  optionaffichage  character varying(3) DEFAULT 111 NOT NULL ,
  optionaqpa  boolean  DEFAULT false NOT NULL ,
  signature  boolean DEFAULT false NOT NULL ,
  CONSTRAINT formalite_pkey PRIMARY KEY (id),
  CONSTRAINT fke369062bdd097f11 FOREIGN KEY (entrepriseid)
      REFERENCES entreprise (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fkformalite321397 FOREIGN KEY (adresseid5)
      REFERENCES adresse (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fkformalite321398 FOREIGN KEY (adresseid4)
      REFERENCES adresse (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fkformalite519942 FOREIGN KEY (observationid)
      REFERENCES observation (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fkformalite593048 FOREIGN KEY (dossierid)
      REFERENCES dossier (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fkformalite595823 FOREIGN KEY (evenementid)
      REFERENCES evenement (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fkformalite61101 FOREIGN KEY (etatid)
      REFERENCES etat (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT signataireadresse FOREIGN KEY (adresseid6)
      REFERENCES adresse (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);



------------------ Création de la table notification_purge ------------------
CREATE TABLE IF NOT EXISTS notification_purge
(
  id bigint NOT NULL,
  numero_dossier character varying(255) NOT NULL,
  etat_id character varying(2) NOT NULL,
  date_suppression_logique date,
  envoye boolean NOT NULL,
  CONSTRAINT notification_purge_pkey PRIMARY KEY (id)
)