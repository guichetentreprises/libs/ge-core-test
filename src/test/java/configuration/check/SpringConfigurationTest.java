/**
 * 
 */
package configuration.check;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Classe permettant de tester le démarrage du contexte Spring.
 * 
 * @author $Author: hhichri $
 * @version $Revision: 0 $
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:/spring/applicationContext-ge-core-test-config.xml" })
public class SpringConfigurationTest {

  /**
   * Test.
   */
  @Test
  public void testSpringConfiguration() {

  }

}
